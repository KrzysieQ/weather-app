import PickCity from '../PickCity/PickCity';
import WeatherSummary from '../WeatherSummary/WeatherSummary';
import Loader from '../Loader/Loader';
import { useCallback, useState } from 'react';
import ErrorBox from '../ErrorBox/ErrorBox';

const WeatherBox = props => {
  const [data, setData] = useState('')
  const [pending, setPending] = useState(false);
  const [err, setErr] = useState('');

  const handleCityChange = useCallback(city => {
    setPending(true);
    fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=d964b0d05ecc29a5505810cdf46801e0&units=metric`)
    .then(res => {
      if(res.status === 200) {
        return res.json()
        .then(data => {
          const weatherData = {
            city: data.name,
            temp: data.main.temp,
            icon: data.weather[0].icon,
            description: data.weather[0].main
          };
          setPending(false);
          setData(weatherData);
          setErr('')
        })
      } else {
        return res.json().then(e => setErr(e.message));
      }
    })
  })

  return (
    <section>
      <PickCity action={handleCityChange} />
      {(data && !err)?(<WeatherSummary {...data}/>):('')}
      {(pending && !err)?(<Loader />):('')}
      {(err)?(<ErrorBox>{err}</ErrorBox>):('')}
    </section>
  )
};

export default WeatherBox;